require('./styles.scss');

import * as React from 'react';
import * as ReactGA from 'react-ga';

import { Person } from '../../../../../lib/interfaces/Person';
import { searchProfiles } from '../../services/orcid';
import { ProfileList } from '../profileList/component';

interface Props { }
interface State {
  profiles?: Array<Partial<Person>> | null;
  query: string;
  searching: boolean;
}

export class ProfileSearch extends React.Component<Props, State> {
  public state: State = {
    profiles: undefined,
    query: '',
    searching: false,
  };

  public constructor(props: Props) {
    super(props);

    this.renderProfiles = this.renderProfiles.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    return (
      <div className="profileSearch">
        <form onSubmit={this.handleSubmit}>
          <div className="field has-addons">
            <div className="control">
              <label
                htmlFor="researcherNameSearchField"
                className="is-sr"
              >
                Researcher name
              </label>
              <input
                onChange={this.handleChange}
                id="researcherNameSearchField"
                placeholder="e.g. 'Jon Tennant'"
                type="search"
                className="input"
              />
            </div>
            <div className="control">
              <button
                type="submit"
                className={'button is-info ' + (this.state.searching ? 'is-loading' : '')}
              >
                Search
              </button>
            </div>
          </div>
        </form>
        <div aria-live="polite">
          {this.renderProfiles()}
        </div>
      </div>
    );
  }

  private renderProfiles() {
    if (typeof this.state.profiles === 'undefined') {
      return null;
    }
    if (this.state.profiles === null) {
      return (
        <div className="message is-danger">
          <div className="message-body">
            There was an error searching for researchers, please try again.
          </div>
        </div>
      );
    }
    if (this.state.profiles.length === 0) {
      return (
        <div className="message is-warning">
          <div className="message-body">
            No researchers found — did you spell their name correctly?
          </div>
        </div>
      );
    }

    return (
      <>
        <ProfileList profiles={this.state.profiles}/>
      </>
    );
  }

  private handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ query: event.target.value });
  }

  private async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.setState({ searching: true });
    ReactGA.event({
      action: 'Search',
      category: 'Profile Search',
    });

    try {
      const profiles = await searchProfiles(this.state.query);

      this.setState({ profiles, searching: false });
    } catch (e) {
      this.setState({ profiles: null, searching: false });
    }
  }
}
