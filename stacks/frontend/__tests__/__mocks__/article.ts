import { Article } from '../../../../lib/interfaces/Article';

export const mockData: Article = {
  author: [
    { name: 'Donald J. Trump' },
    { name: 'Mike Pence' },
  ],
  datePublished: '2017-01-24',
  description: `
Conway’s powerful theory of Alternative Facts can render many difficult problems tractable.
Here we demonstrate the power of AF to prove the Riemann Hypothesis,
one of the most important unsolved problems in mathematics.
  `,
  keywords: [ 'Logic', 'Mathematics' ],
  name: 'Proof of the Riemann Hypothesis utilizing thetheory of Alternative Facts',
  sameAs: 'fake/doi',
  url: 'https://www.scribd.com/document/337471737/' +
    'Proof-of-the-Riemann-Hypothesis-utilizing-the-theory-of-Alternative-Facts',
};
