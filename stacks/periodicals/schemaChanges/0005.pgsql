---------------------------------------------------------
--                      WARNING                        --
-- Do not modify this script after merging it to       --
-- `dev`. Only add new, consecutively numbered         --
-- changesets in case the schema needs further         --
-- changes.                                            --
---------------------------------------------------------

-- NOTE: When running locally. Docker Compose won't pick up changes in SQL files unless you recreate the volume:
--       docker-compose rm -v; docker-compose up --build;

BEGIN;
  CREATE TABLE scholarly_articles_same_as (
    scholarly_article UUID NOT NULL REFERENCES scholarly_articles(identifier),
    same_as TEXT UNIQUE NOT NULL
  );
COMMIT
