import { Periodical } from '../../../../lib/interfaces/Periodical';
import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

export async function fetchPeriodicalContents(
  database: Database,
  periodicalIdentifier: string,
): Promise<Partial<Periodical> & { identifier: string; hasPart: Array<Partial<ScholarlyArticle>>; }> {
  interface Row {
    identifier: string;
    name: string;
    description: string;
    author: string;
    is_part_of: string;
    date_published: Date;
    filename: string;
    file_content_url: string;
  }
  const result = await database.manyOrNone<Row>(
    'SELECT'
      + ' article.identifier, article.name, article.description'
      + ', submission.is_part_of, submission.date_published'
      + ', author.author'
      + ', file.name as filename, file.content_url AS file_content_url'
      + ', submission.is_part_of, submission.date_published'
      + ' FROM periodicals periodical'
      + ' JOIN scholarly_articles_part_of submission ON periodical.uuid=submission.is_part_of'
      + ' JOIN scholarly_articles article ON article.identifier=submission.scholarly_article'
      + ' JOIN scholarly_article_authors author ON author.scholarly_article=article.identifier'
      + ' JOIN scholarly_article_associated_media file ON file.identifier=' + '('
          + 'SELECT identifier FROM scholarly_article_associated_media'
          + ' WHERE scholarly_article=article.identifier'
          + ' ORDER BY date_created DESC'
          + ' LIMIT 1'
        + ')'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' WHERE periodical.identifier=${periodicalId} AND submission.date_published IS NOT NULL',
    {
      periodicalId: periodicalIdentifier,
    },
  );

  interface ArticleMap {
    // The values are going to be `Row`s, but with the `author` replaced by an array of `Person`s.
    // Unfortunately, I don't know how to express this in types, which is why this is an `any` for now.
    [key: string]: any;
  }
  const articles: ArticleMap = result.reduce((soFar: ArticleMap, row: Row) => {
    const newArticle = (soFar[row.identifier]) ? { ...soFar[row.identifier] } : { ...row };
    newArticle.author = (soFar[row.identifier]) ? soFar[row.identifier].author : [];
    newArticle.author.push({ identifier: row.author });

    soFar[row.identifier] = newArticle;

    return soFar;
  }, {});

  const hasPart: Array<Partial<ScholarlyArticle>> =
    Object.keys(articles)
    .map((articleId) => articles[articleId])
    .map((article) => {
      return {
        associatedMedia: [ {
          contentUrl: article.content_url,
          license: 'https://creativecommons.org/licenses/by/4.0/' as 'https://creativecommons.org/licenses/by/4.0/',
          name: article.filename,
        } ],
        author: article.author,
        datePublished: article.date_published.toISOString(),
        description: article.description,
        identifier: article.identifier,
        isPartOf: article.is_part_of,
        name: article.name,
      };
    });

  const periodical = {
    hasPart,
    identifier: periodicalIdentifier,
  };

  return periodical;
}
