jest.mock(
  'node-fetch',
  () => ({
    default: jest.fn().mockReturnValue(Promise.resolve({
      json: () => Promise.resolve({ data: 'arbitrary orcid data' }),
    })),
  }),
);

import { readOrcid } from '../../src/resources/readOrcid';

const mockContext = {
  body: {},

  headers: {},
  method: 'GET' as 'GET',
  params: [ '/orcid/arbitrary_orcid', 'arbitrary_orcid' ],
  path: '/orcid/arbitrary_orcid',
  query: null,
};

beforeEach(() => {
  process.env.orcid_base_path = 'https://arbitrary.orcid.basepath';
});

it('should return the ORCID response when passed the correct values', () => {
  return expect(readOrcid(mockContext)).resolves.toHaveProperty('data');
});

it('should error when no ORCID was specified', () => {
  return expect(readOrcid({
    ...mockContext,
    params: [],
    path: '/',
  })).rejects
    .toEqual(new Error('Please specify an ORCID to fetch the public profile of.'));
});

it('should error when the ORCID API returns an error', () => {
  const mockedFetch = require.requireMock('node-fetch').default.mockReturnValueOnce(
    Promise.resolve({ json: () => Promise.resolve({ error: 'Arbitrary error' }) }),
  );

  return expect(readOrcid(mockContext)).rejects
    .toEqual(new Error('Could not fetch this ORCID profile, please try again.'));
});

it('should call to the sandbox public API when the member API is also pointing to the sandbox', () => {
  const mockedFetch = require.requireMock('node-fetch').default;
  process.env.orcid_base_path = 'https://arbitrary.sandbox.orcid.org.basepath';

  readOrcid(mockContext);

  expect(mockedFetch.mock.calls[0][0]).toMatch('.sandbox');
});
