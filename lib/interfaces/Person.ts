export interface Person {
  identifier: string;
  description?: string;
  sameAs?: SameAs | SameAs[];
  name?: string;
  affliation?: AffliationRole[];
}

type SameAs = string | SameAsRole;

interface SameAsRole extends Role {
  sameAs: string;
}

interface AffliationRole extends Role {
  affliation: {
    name: string;
  };
}

// https://schema.org/Role
interface Role {
  startDate?: string;
  endDate?: string;
  roleName?: string;
}
